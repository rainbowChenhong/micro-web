/*
 * @Description:
 * @version: 1.0.0
 * @Author: chenhong
 * @Date: 2021-07-19 11:48:06
 * @LastEditors: chenhong
 * @LastEditTime: 2021-07-19 13:21:01
 */
import React from "react";
let publicPath = "/react-web/";
const PublicPathContext = React.createContext(
  window.__POWERED_BY_QIANKUN__ ? publicPath : "/"
);
export {publicPath};
export default PublicPathContext;
