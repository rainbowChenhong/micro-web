import "./App.css";
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import PublicPathContext from "./PublicPathContext";
import img from "./assert/1.jpeg";
function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}
function App(props) {
  const {publicPath} = props;
  return (
    <div className="App">
      <h3>react子应用</h3>
      <img src={img} alt="logo" width="200px" />
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to={`${publicPath}`}>Home</Link>
              </li>
              <li>
                <Link to={`${publicPath}about`}>About</Link>
              </li>
              <li>
                <Link to={`${publicPath}users`}>Users</Link>
              </li>
            </ul>
          </nav>

          {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
          <Switch>
            <Route path={`${publicPath}about`}>
              <About />
            </Route>
            <Route path={`${publicPath}users`}>
              <Users />
            </Route>
            <Route path={`${publicPath}`}>
              <Home />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}
function ContextApp() {
  return (
    <PublicPathContext.Consumer>
      {(path) => <App publicPath={path} />}
    </PublicPathContext.Consumer>
  );
}
export default ContextApp;
