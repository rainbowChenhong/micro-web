/*
 * @Description:
 * @version: 1.0.0
 * @Author: chenhong
 * @Date: 2021-07-17 16:26:43
 * @LastEditors: chenhong
 * @LastEditTime: 2021-07-17 16:54:37
 */
const {name} = require("./package");

module.exports = {
  webpack: (config) => {
    config.output.library = `${name}-[name]`;
    config.output.libraryTarget = "umd";
    config.output.jsonpFunction = `webpackJsonp_${name}`;
    config.output.globalObject = "window";
    return config;
  },

  devServer: (_) => {
    const config = _;

    config.headers = {
      "Access-Control-Allow-Origin": "*",
    };
    config.historyApiFallback = true;
    config.hot = false;
    config.watchContentBase = false;
    config.liveReload = false;
    return config;
  },
};
