/*
 * @Description:
 * @version: 1.0.0
 * @Author: chenhong
 * @Date: 2021-07-19 10:01:21
 * @LastEditors: chenhong
 * @LastEditTime: 2021-07-19 11:12:08
 */
import Vue from "vue";
import VueRouter from "vue-router";
// const Foo = {template: "<div>foo</div>"};
// const Bar = {template: "<div>bar</div>"};
import HelloWorld from "./components/HelloWorld";
import Foo from "./components/foo";
const routes = [
  {path: "/foo", component: Foo},
  //   {path: "/bar", component: Bar},
  {path: "/hello", component: HelloWorld},
];
Vue.use(VueRouter);

export default routes;
