import Vue from "vue";
import App from "./App.vue";
import routes from "./router";
import VueRouter from "vue-router";
Vue.config.productionTip = false;
let basePath = "/vue-test";
let router = null;
let instance = null;
function render(props = {}) {
  const {container} = props;
  router = new VueRouter({
    base: window.__POWERED_BY_QIANKUN__ ? basePath : "/",
    mode: "history",
    routes: routes,
  });
  router.beforeEach((to, from, next) => {
    //避免死循环
    if (window.__POWERED_BY_QIANKUN__ && to.path.indexOf(basePath) < -1) {
      next(`${basePath}${to.path}`);
      return;
    } else {
      next();
    }
  });
  instance = new Vue({
    router,
    render: (h) => h(App),
  }).$mount(container ? container.querySelector("#app") : "#app");
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  render();
}

export async function bootstrap() {
  console.log("[vue] vue app bootstraped");
}
export async function mount(props) {
  console.log("[vue] props from main framework", props);
  render(props);
  props.setGlobalState({loginName: "vue项目-陈女士"});
}
export async function unmount() {
  instance.$destroy();
  instance.$el.innerHTML = "";
  instance = null;
  router = null;
}
