/*
 * @Description:
 * @version: 1.0.0
 * @Author: chenhong
 * @Date: 2021-07-15 17:09:26
 * @LastEditors: chenhong
 * @LastEditTime: 2021-07-19 10:17:23
 */
const {name} = require("./package");
const publicPath =
  process.env.NODE_ENV === "production"
    ? "https://qiankun.umijs.org/"
    : `http://localhost:8080`;
module.exports = {
  devServer: {
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  },

  configureWebpack: {
    output: {
      library: `${name}-[name]`,
      libraryTarget: "umd", // 把微应用打包成 umd 库格式
      jsonpFunction: `webpackJsonp_${name}`,
    },
  },
  chainWebpack: (config) => {
    config.module
      .rule("fonts")
      .use("url-loader")
      .loader("url-loader")
      .options({
        limit: 4096, // 小于4kb将会被打包成 base64
        fallback: {
          loader: "file-loader",
          options: {
            name: "fonts/[name].[hash:8].[ext]",
            publicPath,
          },
        },
      })
      .end();
    config.module
      .rule("images")
      .use("url-loader")
      .loader("url-loader")
      .options({
        limit: 4096, // 小于4kb将会被打包成 base64
        fallback: {
          loader: "file-loader",
          options: {
            name: "img/[name].[hash:8].[ext]",
            publicPath,
          },
        },
      });
  },
};
