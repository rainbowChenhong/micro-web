import "./App.css";
import {initGlobalState} from "qiankun";
import {useState} from "react";
let state = {loginName: "chen女士"};
const actions = initGlobalState(state);

function App() {
  const [name, setName] = useState(state);
  function changeTab(subapp) {
    window.history.pushState(null, subapp, subapp);
  }
  actions.onGlobalStateChange((state, prev) => {
    // state: 变更后的状态; prev 变更前的状态
    console.log(state, prev);
    setName(state);
  });
  return (
    <div className="container-box">
      <div className="menu">
        <div onClick={() => changeTab("/react-web")}>react-web项目</div>
        <div onClick={() => changeTab("/vue-test")}>vue-test项目</div>
        <div>登录名：{name && name.loginName}</div>
        {/* <div onClick={() => changeTab("/app-vue")}>vue项目</div> */}
        {/* <a href="/app-vue">vue项目</a>
        <a href="/vue-test">vue-test项目</a>
        <a href="/react-web">react-web项目</a> */}
      </div>
      <div id="container"></div>
    </div>
  );
}

export default App;
