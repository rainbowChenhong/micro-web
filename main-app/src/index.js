import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import {registerMicroApps, start} from "qiankun";

registerMicroApps([
  // {
  //   name: "xpms",
  //   entry: "http://localhost:8002",
  //   container: "#container",
  //   activeRule: "/app-vue",
  // },
  {
    name: "vue-test",
    entry: "http://localhost:8080/",
    container: "#container",
    activeRule: "/vue-test",
  },
  {
    name: "react-web",
    entry: "http://localhost:3001",
    container: "#container",
    activeRule: "/react-web",
  },
]);

// 启动 qiankun
start();
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// 初始化 state

// actions.setGlobalState(state);
reportWebVitals();
